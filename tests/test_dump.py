from StringIO import StringIO
import unittest
import os
import sys
import json

sys.path.append(os.path.realpath('../'))

import logview


class TestDump(unittest.TestCase):

    def setUp(self):
        self.f = StringIO()
        self.d = logview.LogParser()

    def escrever(self, s):
        self.f = StringIO(s)

    def test_obterHierarquiaEmJson(self):
        self.escrever("""
        [0:23596274] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [1:23615072] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [2:23615212] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [3:23615212] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [4:23615337] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [5:23615353] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [6:23630984] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [7:23630984] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [8:23631000] - EventoDeSistema.Disparar(IniciarDependencias) inicio
        [9:23631015] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [10:23631031] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [11:23631047] - EventoDeSistema.Disparar(IniciarDependencias) fim
        """)

        esperado = {"name": "log", "children": [
            {
                "name": "EventoDeSistema.Disparar(AtualizarSplash)",
                "children": [
                    {"name": 0, "size": 18798},
                    {"name": 18938, "size": 0}, 
                    {"name": 19063, "size": 16},
                    {"name": 34710, "size": 0},
                    {"name": 34741, "size": 16}
                ]
            },
            {
                "name": "EventoDeSistema.Disparar(IniciarDependencias)",
                "children": [
                    {"name": 34726, "size": 47}
                ]
            }
        ]}

        #esperado_json = json.dumps(esperado)

        self.d.parse(self.f)

        series = self.d.timeseries()

        hierarchy = logview.dump.hierarchy(series)
