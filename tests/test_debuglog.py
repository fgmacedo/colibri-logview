from StringIO import StringIO
import unittest
import os
import sys

sys.path.append(os.path.realpath('../'))

import logview


class TestLogParser(unittest.TestCase):

    def setUp(self):
        self.f = StringIO()
        self.d = logview.LogParser()

    def escrever(self, s):
        self.f = StringIO(s)

    def test_encontrarEvento(self):
        self.escrever("""
        [0:23596274] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [1:23615072] - EventoDeSistema.Disparar(AtualizarSplash) fim
        """)

        self.d.parse(self.f)
        self.assertEqual(len(self.d), 2)

    def test_extrairAtributosDaLinha(self):
        self.escrever("""
        [2:23596274] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [1:23615072] - EventoDeSistema.Disparar(AtualizarSplash) fim
        """)

        self.d.parse(self.f)
        i1 = self.d[0]
        self.assertEqual(i1.id, 2)
        self.assertEqual(i1.time, 0)
        self.assertEqual(i1.name, 'EventoDeSistema.Disparar(AtualizarSplash)')
        self.assertEqual(i1.type, 'inicio')

    def test_obterItensFilhoEmPrimeiroNivel(self):
        self.escrever("""
        [0:0] - A inicio
        [1:1] - B inicio
        [2:2] - B fim
        [3:3] - C inicio
        [4:4] - C fim
        [5:5] - D inicio
        [6:6] - D fim
        [7:7] - A fim
        """)

        self.d.parse(self.f)
        self.assertEqual(len(self.d.timers), 1)

        expected = [
            logview.LogTimer(logview.Storage(id=1, name="B", time=1, children=[]), logview.Storage(id=2, name="B", time=2, children=[])),
            logview.LogTimer(logview.Storage(id=3, name="C", time=3, children=[]), logview.Storage(id=4, name="C", time=4, children=[])),
            logview.LogTimer(logview.Storage(id=5, name="D", time=5, children=[]), logview.Storage(id=6, name="D", time=6, children=[])),
        ]

        children = self.d.timers[0].children
        self.assertEqual(children, expected)

    def test_obterItensFilhoEmSegundoNivel(self):
        self.escrever("""
        [0:0] - A inicio
        [1:1] - B inicio
        [2:2] - C inicio
        [3:3] - C fim
        [4:4] - D inicio
        [5:5] - D fim
        [6:6] - B fim
        [7:7] - A fim
        [8:8] - E inicio
        [9:9] - E fim
        """)

        self.d.parse(self.f)
        self.assertEqual(len(self.d.timers), 2)

        expected = [
            logview.LogTimer(logview.Storage(id=1, name="B", time=1, children=[
                logview.LogTimer(logview.Storage(id=2, name="C", time=2, children=[]), logview.Storage(id=3, name="C", time=3, children=[])),
                logview.LogTimer(logview.Storage(id=4, name="D", time=4, children=[]), logview.Storage(id=5, name="D", time=5, children=[]))
            ]), logview.Storage(id=6, name="B", time=6, children=[])),
        ]

        children = self.d.timers[0].children
        self.assertEqual(children, expected)


    def test_obterSerieDeTempos(self):
        self.escrever("""
        [0:23596274] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [1:23615072] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [2:23615212] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [3:23615212] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [4:23615337] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [5:23615353] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [6:23630984] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [7:23630984] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [8:23631000] - EventoDeSistema.Disparar(IniciarDependencias) inicio
        [9:23631015] - EventoDeSistema.Disparar(AtualizarSplash) inicio
        [10:23631031] - EventoDeSistema.Disparar(AtualizarSplash) fim
        [11:23631047] - EventoDeSistema.Disparar(IniciarDependencias) fim
        """)

        self.d.parse(self.f)

        series = self.d.timeseries()
        self.assertEqual(len(series), 2)
        self.assertEqual(series[0].name, 'EventoDeSistema.Disparar(AtualizarSplash)')
        self.assertEqual(series[0].values, [(0, 18798), (18938, 0), (19063, 16), (34710, 0)])
        self.assertEqual(series[0].duration, 18830)
        self.assertEqual(series[1].name, 'EventoDeSistema.Disparar(IniciarDependencias)')
        self.assertEqual(series[1].values, [(34726, 47)])
        self.assertEqual(series[1].duration, 47)
