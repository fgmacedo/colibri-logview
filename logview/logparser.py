import re

class Storage(dict):

    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        self[name] = value


class LogItem(Storage):
    def __init__(self, match):
        self.id = int(match[0])
        self.time = int(match[1])
        self.name = match[2]
        self.type = match[3]
        self.children = []

    def __repr__(self):
        return "<LogItem %d:%d - %s>" % (self.id, self.time, self.name,)


class LogTimer(Storage):
    def __init__(self, itemStart, itemEnd):
        self.name = itemStart.name
        self.idBegin = itemStart.id
        self.idEnd = itemEnd.id
        self.timeBegin = itemStart.time
        self.timeEnd = itemEnd.time
        self.duration = self.timeEnd - self.timeBegin
        self.children = itemStart.children

    def __repr__(self):
        return "<LogTimer %s - %dms>" % (self.name, self.duration,)


class TimeSerie(Storage):
    def __init__(self, name):
        self.name = name
        self.values = []
        self.duration = 0

    def addValue(self, timeBegin, value):
        self['values'].append((timeBegin, value,))
        self.duration = self.duration  + value

    @property
    def values(self):
        return self['values']

    def __repr__(self):
        return "<TimeSerie %s - %d values>" % (self.name, len(self.values),)


class LogParser(object):
    def __init__(self):
        self.items = []
        self.timers = []
        self.reobj = re.compile(r"\[(\d+):(\d+)\] - (.*) (inicio|fim)", re.IGNORECASE | re.MULTILINE)

    def parse_file(self, file_name):
        with open(file_name, 'r') as f:
            self.parse(f)

    def parse(self, f):
        queue = []
        firstTickCount = 0
        for l in f:
            r = self.reobj.findall(l)
            if not r:
                continue
            item = LogItem(r[0])

            if len(self.items) == 0:
                firstTickCount = item.time
                item.time = 0
            else:
                item.time -= firstTickCount

            self.items.append(item)

            if item.type == 'inicio':
                queue.append(item)
            elif item.type == 'fim':
                earlierItem = queue.pop()
                timer = LogTimer(earlierItem, item)

                # insert as a child if there's an item queued
                parent = queue[-1:]
                if parent:
                    parent[0].children.append(timer)
                else:
                    self.timers.append(timer)

    def timeseries(self):
        series = []
        seriesdict = {}
        for time in self.timers:
            if time.name in seriesdict:
                seriesdict[time.name].addValue(time.timeBegin, time.duration)
            else:
                serie = TimeSerie(time.name)
                series.append(serie)
                seriesdict[time.name] = serie
                serie.addValue(time.timeBegin, time.duration)

        return series

    def __repr__(self):
        return "<LogParser %d items>" % (len(self.items),)

    def __len__(self):
        return len(self.items)

    def __getitem__(self, index):
        return self.items[index]
