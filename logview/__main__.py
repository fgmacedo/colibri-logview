from logparser import LogParser

if __name__ == '__main__':
    import sys
    import json
    if len(sys.argv) != 2:
        pass

    p = LogParser()
    p.parse_file(sys.argv[1])


    print json.dumps({"name": "log", "children": p.timers})